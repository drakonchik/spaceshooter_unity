﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BossBattle : MonoBehaviour 
{

	public GameObject enemyBullet;

	List<GameObject> EnemyBulletList = new List<GameObject>();

	public GameObject Boss;

	public int angleOffset;

	public int MaxSpiralUnits = 10;

	// Use this for initialization
	void Start () 
	{
		Boss = GameObject.FindGameObjectWithTag("Boss");
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
