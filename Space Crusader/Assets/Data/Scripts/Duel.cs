﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Duel : MonoBehaviour 
{
	
	public GameObject enemyBullet;

	List<GameObject> EnemyBulletList = new List<GameObject>();

	public int angleOffset;
	public int MaxSpiralUnits = 10;

	public int  EnemyHealth = 100;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine ("SpiratAttack");
	}

	// Update is called once per frame
	void Update () 
	{
		/*if (EnemyHealth < 80) 
		{
			transform.position += new Vector3 (0, -0.2f, 0);
		}*/

	}



	IEnumerator SpiratAttack()
	{
		const float dtheta = (float)(5 * Mathf.PI / 180);    // Five degrees.
		for (float theta = 0;; theta += dtheta) 
		{

			// Calculate r.
			float r = 0.34f * theta;

			// Convert to Cartesian coordinates.
			float x, y;

			x = (float)(r * Mathf.Cos ((theta + angleOffset)));
			y = (float)(r * Mathf.Sin ((theta + angleOffset)));

			// Center.
			x += transform.position.x;
			y += transform.position.y;

			// Create the point.
			EnemyBulletList.Add((GameObject)Instantiate(enemyBullet,new Vector3(x, y, 0),Quaternion.identity));
			yield return new WaitForEndOfFrame ();
			// If we have gone far enough, stop.
			if (r > MaxSpiralUnits)
				break;
		}
	}

	/*void SinWave()
	{

	}*/
}
