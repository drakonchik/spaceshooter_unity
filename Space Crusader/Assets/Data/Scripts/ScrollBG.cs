﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class ScrollBG : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position += new Vector3 (0, -0.4f, 0);

		if (transform.position.y < -500) 
		{
			SceneManager.LoadScene("Data/Scenes/BossBattle");
		}
	}
}
